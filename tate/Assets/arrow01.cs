﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrow01 : MonoBehaviour
{
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.1f, 0, 0);  //等速で落下させる

        if (transform.position.x< -9.0f)  //画面外に出たらオブジェクトを破棄する
        {
            Destroy(gameObject);
        }

        //当たり判定
        Vector2 p1 = transform.position;  //矢の中心座標
        Vector2 p2 = player.transform.position;  //プレイヤの中心座標
        Vector2 dir = p1 - p2;//プレイヤから矢の距離
        float d = dir.magnitude;
        float r1 = 0.5f;  //矢の半径
        float r2 = 1.0f;  //プレイヤの半径

        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();
            Destroy(gameObject);  //衝突した場合は矢を消す
        }
    }
}
