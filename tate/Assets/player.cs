﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
      
    }
  
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))  //上矢印のキーを押したとき
        {
            transform.Translate(0, 3, 0);  //上に3動く
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))  //下矢印のキーを押したとき
        {
            transform.Translate(0, -3, 0);  //下に3動く
        }
    }
}
